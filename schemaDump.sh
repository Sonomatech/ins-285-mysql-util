#!/bin/bash

# tables & views
mysqldump --user=root --password=$MYSQL_ROOT_PASSWORD --no-data --skip-comments --tab=/mysql-util/schema/tables $MYSQL_DATABASE

# move views
xargs -r0 --arg-file <(find /mysql-util/schema/tables -type f -exec grep -lZI 'DROP VIEW IF EXISTS' {} +) mv -f --target-directory /mysql-util/schema/views

# routines
mysqldump --user=root --password=$MYSQL_ROOT_PASSWORD -n -d -t --routines --triggers --skip-comments $MYSQL_DATABASE | perl /mysql-util/separateRoutines.pl