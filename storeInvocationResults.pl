#!/usr/bin/perl

use strict;
use warnings;
use utf8;

use DBI;
use Data::Dumper;
use Text::CSV;

# @TODO separate info for test and target dbs
my $num_args = $#ARGV + 1;
if ($num_args != 5) {
    print "\nUsage: storeInvocationResults.pl dbhost dbname dbuser dbpass resultsdirpath\n";
    exit;
}

my $dbhost = $ARGV[0];
my $dbname = $ARGV[1];
my $dbuser = $ARGV[2];
my $dbpass = $ARGV[3];
my $resultsdirpath = $ARGV[4];

my $gmtestdsn = "DBI:mysql:database=$dbname;host=$dbhost;port=3306";
my $gmtestdbh = DBI->connect($gmtestdsn, $dbuser, $dbpass);

my $insightdsn = "DBI:mysql:database=insightapp;host=$dbhost;port=3306";
my $insightdbh = DBI->connect($insightdsn, $dbuser, $dbpass);

my $sth = $gmtestdbh->prepare("
	SELECT i.id, i.sqltext, p.proc
	FROM gmtest.invocation i
	LEFT JOIN gmtest.proc p
	ON (i.proc_id = p.id)
	LEFT JOIN proctype p2
		on p.proctype_id = p2.id
	WHERE p2.name = 'READ'
") or die "prepare select invocations failed: $gmtestdbh->errstr()";

$sth->execute()
	or die "Couldn't execute statement: ". $sth->errstr;

my @skipped = ();
while(my $row = $sth->fetchrow_hashref) {
	print "Executing ".$row->{proc}."\n";

	my $proch = $insightdbh->prepare($row->{sqltext});
	$proch->execute();

	my @results = @{$proch->fetchall_arrayref};

	if (scalar @results == 0) {
		push @skipped, $row->{id};
		print sprintf("%d-%s SKIPPED FOR NO RESULTS\n", $row->{id}, $row->{proc});
		next;
	}

	my $resultfilepath = sprintf("%s/%d-%s.csv", $resultsdirpath, $row->{id}, $row->{proc});

	my $csv = Text::CSV->new({ binary => 1, auto_diag => 1});

	open my $fh, '>', $resultfilepath or die "could not open $resultfilepath: $!";
	$csv->say($fh, $proch->{NAME_lc});
	$csv->say($fh, $_) for @results;
	close $fh or die "could not close $resultfilepath: $!";

}

print "\n".join(',', @skipped)."\n";