#!/usr/bin/perl
# https://gist.github.com/temmings/c6599ff6a04738185596
# mysqldump -routines --no-create-info --no-data --no-create-db --compact | perl $0

use strict;
use warnings;
use utf8;

my $output_dir = "/mysql-util/schema/";

sub getProcName() {
    (my $name = $_) =~ s/^CREATE\s+([^\s]+)\s+`([^\s`(]+)`\s*\(?.*/$2/eg;
    return $name;
}

## main
my %procs = ();

my $proc_name = "";
my $delimiter = ';';

while (<>) {
    if (/^DELIMITER\s+/) {
        $delimiter = $_;
        $delimiter =~ s/^DELIMITER\s+(.+)$/$1/eg;
    }

    if (/^CREATE\s+/) {
        if (/DEFINER=/) {
            # remove DEFINER
            s/^(.+\s+)DEFINER=`[^\s]+`\s+(.+)$/$1$2/;
        }
        if (/^CREATE\s+(FUNCTION|PROCEDURE)/) {
            $proc_name = lc("$1s/") . &getProcName($_);
            # append DELIMITER to first line
            $procs{$proc_name} .= "DELIMITER $delimiter\n";
        }
    }

    if ($proc_name) {
        $procs{$proc_name} .= $_;
    }

    if (/END\s+$delimiter/) {
        $proc_name = "";
    }
}

# store files
mkdir($output_dir);
for (keys %procs) {
    my $key = $_;
    chomp;
    my $filename = "$output_dir/$_.sql";
    print "Dump $_ -> $filename\n";
    open(my $fh, '>', $filename) or die "Could not open file '$filename' $!";
    print $fh $procs{$key};
    close $fh;
}

print "done\n";