#!/bin/bash

USAGE="dbmirror.sh {source_host} {source_username} {source_password} {source_db_name} {dest_username} {dest_password} {dest_db_name}";
if [ $# -ne 7 ]; then
  echo "Usage:";
	echo $USAGE;
	echo "You can retrieve the host and credential information from the Field Ops KeePass"
	echo "If grabbing dev info, remember to get the MySQL Account"
	exit 1;
fi

SOURCE_HOST=$1;
SOURCE_USERNAME=$2;
SOURCE_PASSWORD=$3;
SOURCE_DB_NAME=$4;

DEST_USERNAME=$5;
DEST_PASSWORD=$6;
DEST_DB_NAME=$7;

TABLE_LIST=("Agency" "DataStream" "DataStreamAggregate" "Instrument" "InstrumentType" "OpCode" "Parameter" "ParameterAggregate" "QCCheck" "QCTask" "Site" "Unit")

for ((i=0; i<${#TABLE_LIST[@]}; i++));
do
  TABLE_NAME=${TABLE_LIST[$i]};
  rm -f $TABLE_NAME.sql

  echo "Dumping $TABLE_NAME from $SOURCE_HOST";
  mysqldump --host=$SOURCE_HOST --user=$SOURCE_USERNAME --password=$SOURCE_PASSWORD -c $SOURCE_DB_NAME $TABLE_NAME > /mysql-util/data/$TABLE_NAME.sql;

  if [ $? -ne 0 ]; then
    echo "Failed to dump $TABLE_NAME";
    exit $?
  fi

  echo "Importing $TABLE_NAME to localhost";
  mysql --user=$DEST_USERNAME --password=$DEST_PASSWORD $DEST_DB_NAME < /mysql-util/data/$TABLE_NAME.sql
  if [ $? -ne 0 ]; then
    echo "Failed to import $TABLE_NAME";
    exit $?
  fi

done

