#!/usr/bin/perl

use strict;
use warnings;
use utf8;

use DBI;

my $num_args = $#ARGV + 1;
if ($num_args != 5) {
    print "\nUsage: insertInvocations.pl dbhost dbname dbuser dbpass filepath\n";
    exit;
}

my $dbhost = $ARGV[0];
my $dbname = $ARGV[1];
my $dbuser = $ARGV[2];
my $dbpass = $ARGV[3];
my $filepath = $ARGV[4];

my $dsn = "DBI:mysql:database=$dbname;host=$dbhost;port=3306";
my $dbh = DBI->connect($dsn, $dbuser, $dbpass);

my $sth = $dbh->prepare("
	INSERT INTO gmtest.invocation (sqltext, proc_id)
	SELECT ?, p.id
	FROM gmtest.proc p
	LEFT JOIN gmtest.proctype pt
	ON (p.proctype_id = pt.id)
	WHERE p.proc = ?
") or die "prepare insert invocation failed: $dbh->errstr()";

open(FH, '<', $filepath) or die $!;

while(<FH>) {
	my $sqltext = $_;
	$sqltext =~ /call (\w+)\(/;

	$sth->execute($sqltext, $1);

	print "\nInserted $1 | $sqltext";
}
print "\n";
