#!/bin/bash

USAGE="ins285.sh {source_host} {source_username} {source_password} {source_db_name}";
if [ $# -ne 4 ]; then
  echo "Usage:";
	echo $USAGE;
	echo "You can retrieve the host and credential information from the Field Ops KeePass"
	echo "If grabbing dev info, remember to get the MySQL Account"
	exit 1;
fi

SOURCE_HOST=$1;
SOURCE_USERNAME=$2;
SOURCE_PASSWORD=$3;
SOURCE_DB_NAME=$4;

# explicitly excluding id_lookup, test, and text_lookup tables
#
TABLE_LIST=("Agency" "AgencyType" "Aggregate" "AggregateValueOverrides" "AlertGroup" "AlertGroupEmails"
"AlertGroupSubscribers" "AlertTemplate" "ApiQueryConfiguration" "Country" "DataBulkUpdate" "DataLake" "DataStream"
"DataStreamAggregate" "DataStreamList" "DataStreamListEvent" "DataStreamListItem" "DataStreamStats" "Debug" "Duration"
"Export" "ExportQueue" "Frequency" "Group" "GroupRights" "GroupType" "GroupUsers" "Instrument" "InstrumentType"
"InstrumentTypeDefaultAggregate" "InstrumentTypeDefaultQCTask" "Job" "JobData" "MapMarkerStyle" "OpCode" "Operator"
"Parameter" "ParameterAggregate" "ParameterColorRamp" "ParameterGroup" "ParameterGroupParameters" "ParameterType"
"ParameterTypeColorRamp" "Plugin" "PluginAgency" "QCCheck" "QCCode" "QCTask" "QCTaskAlertGroups" "QCTaskAlertItem"
"QCTaskAlertItemAlertGroups" "QCTaskAlertOperatorResponse" "QCTaskAlertTemplate" "QCTaskType" "Region" "Right"
"Site" "SiteList" "SiteListSites" "SiteLog" "State" "TimeZone" "UIGraph" "UIGraphSeries" "Unit" "UnitConversionDetail"
"User" "UserPlugins")

for ((i=0; i<${#TABLE_LIST[@]}; i++));
do
  TABLE_NAME=${TABLE_LIST[$i]};
  rm -f $TABLE_NAME.sql

  echo "Dumping $TABLE_NAME from $SOURCE_HOST";
  mysqldump --quick --host=$SOURCE_HOST --user=$SOURCE_USERNAME --password=$SOURCE_PASSWORD --quick -c $SOURCE_DB_NAME $TABLE_NAME > /mysql-util/data/$TABLE_NAME.sql;

  if [ $? -ne 0 ]; then
    echo "Failed to dump $TABLE_NAME";
    exit $?
  fi
  echo "Done"
done

echo "Dumping EventLog from $SOURCE_HOST";
mysqldump --host=$SOURCE_HOST --user=$SOURCE_USERNAME --password=$SOURCE_PASSWORD --quick -c $SOURCE_DB_NAME EventLog --where="UTC BETWEEN '2020-04-01 00:00:00' AND '2020-07-01 00:00:00'" > /mysql-util/data/EventLog.sql
echo "Done"

echo "Dumping Measurement from $SOURCE_HOST";
mysqldump --host=$SOURCE_HOST --user=$SOURCE_USERNAME --password=$SOURCE_PASSWORD --quick -c $SOURCE_DB_NAME Measurement --where="UTC BETWEEN '2020-04-01 00:00:00' AND '2020-07-01 00:00:00'" > /mysql-util/data/Measurement.sql
echo "Done"

echo "Dumping QCLog from $SOURCE_HOST";
mysqldump --host=$SOURCE_HOST --user=$SOURCE_USERNAME --password=$SOURCE_PASSWORD --quick -c $SOURCE_DB_NAME QCLog --where="UTC BETWEEN '2020-04-01 00:00:00' AND '2020-07-01 00:00:00'" > /mysql-util/data/QCLog.sql
echo "Done"

echo "Dumping QCLogData from $SOURCE_HOST";
mysqldump --host=$SOURCE_HOST --user=$SOURCE_USERNAME --password=$SOURCE_PASSWORD --quick -c $SOURCE_DB_NAME QCLogData --where="UTC BETWEEN '2020-04-01 00:00:00' AND '2020-07-01 00:00:00'" > /mysql-util/data/QCLogData.sql
echo "Done"

echo "Dumping QCLogDataCheck from $SOURCE_HOST";
mysqldump --host=$SOURCE_HOST --user=$SOURCE_USERNAME --password=$SOURCE_PASSWORD --quick -c $SOURCE_DB_NAME QCLogDataCheck --where="CreatedUTC BETWEEN '2020-04-01 00:00:00' AND '2020-07-01 00:00:00'" > /mysql-util/data/QCLogDataCheck.sql
echo "Done"